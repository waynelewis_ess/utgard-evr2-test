#!../../bin/linux-x86_64/evr2_test
epicsEnvSet("SYS", "LabS-Utgard-VIP:TS")
epicsEnvSet("PCI_SLOT", "1:0.0")
epicsEnvSet("EVR", "EVR-2")
epicsEnvSet("DEVICE", "$(EVR)")

epicsEnvSet("SYS2","LabS-Utgard-VIP:")
epicsEnvSet("EVR2","TS-EVR-2:")
epicsEnvSet("DEV1","Chop1:")
epicsEnvSet("DEV2","Chop2:")
epicsEnvSet("DEV3","Chop3:")
epicsEnvSet("DEV4","Chop4:")
epicsEnvSet("CHICSYS", "LabS-Utgard-VIP")
epicsEnvSet("CHICID", "Chop-CHIC-02")


< "st.evr.cmd"
